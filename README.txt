Enabling this module will show a javascript confirm box that verifies that the user wants to close out the overlay and may lose changes.

Requirements: Drupal 7.x
