(function ($) {
  // Invokes Drupal Behaviours.
  Drupal.behaviors.overlay_close_warning = {
    attach: function (context, settings) {
      // Present a confirm box that confirms close onclick.
      $('#overlay-close-wrapper').find('#overlay-close').click('drupalOverlayBeforeClose', function (e) {
        confirmation = confirm(Drupal.t('Do you want to close this window? Any unsaved Changes will be lost.'));
        // If the user choses cancel, do not close the overlay.
        if (!confirmation) {return false;}
      })
    }
  };

})(jQuery);
